#!/usr/bin/env python

import argparse 

from subprocess import getoutput
from os import listdir
#from natsort import natsorted
import concurrent.futures
import time
from multiprocessing import cpu_count
import os

results = []
execution_times = []

def main():
    args = parse_cmdline()
    test_set = read_csvs(args.csv)
    print(test_set)
    args.model = args.deepspeech_path + args.model
    args.alphabet = args.deepspeech_path + args.alphabet
    args.lm = args.deepspeech_path + args.lm
    args.trie  = args.deepspeech_path + args.trie
    #del test_set[0] #remove header: wav_filename,wav_filesize, transcript

    def run_deepspeech(args, test):
        start = time.time()
        deepspeech_tmplate = "deepspeech --model {model} --alphabet {alphabet} --lm {lm}  --trie {trie} --audio {audio}"
        deepspeech_cmd = deepspeech_tmplate.format(model=args.model, alphabet=args.alphabet, lm=args.lm, trie=args.trie, audio = test[0])
        stdoutdata = getoutput(deepspeech_cmd)
        linified = str(stdoutdata).splitlines()
        inference = linified[len(linified) - 1]
        # results(filename_wav, ground_truth, inference)
        results.append((test[0], test[2], inference))
        end = time.time()
        execution_times.append(end - start)
        return(str(test[2]) +"\n" + inference)
    
    # We can use a with statement to ensure threads are cleaned up promptly
    max_workers = cpu_count() # num of threads per core
    max_workers = 8 * 2
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        # Start the load operations and mark each future with its URL
        start = time.time()
        print("Workers:" + str(max_workers))
        future_deepspeech = {executor.submit(
            run_deepspeech, args, test): test for test in test_set}
        for future in concurrent.futures.as_completed(future_deepspeech):
            try:
                print(future.result())
            except Exception as exc:
                print('generated an exception' + str(exc))
            else:
                pass
                #print("Completed work")
        end = time.time()
        print("execution time: " + str(end - start))
        print("total execution time: " + str(sum(execution_times)))

    print("writing to file")
    with open(args.output, 'w') as out:
        out.write("filename_wav, ground_truth, inference\n\r")
        out.write('\n'.join('%s,%s,%s' % x for x in results))
    print("Finished writing to file")



def read_csvs(csv_files):
    source_data = []
    with open(csv_files, 'r') as csv:
        for e, line in enumerate(csv):
            if  e == 0: # ignore header
                pass
            else:
                source_data.append(line.replace('\n', '').split(','))
    return source_data


def parse_cmdline():
    parser = argparse.ArgumentParser(
        description='Runs deepspeech with pretrained model on a set of audio files and returns a file containing the \
deepspeech speech-to-text results. Must be exectued within deepspeech root dir',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('--csv', help='csv file according to deepspeech expected format. For inference and evaulation.',
                       default='synthesized_wavs.csv')

    parser.add_argument('--output', help='csv file containing filename, ground_truth, inference',
        default='parallel_output.csv')
    parser.add_argument('--lm', help='Absolute path to language model.', default='lm.binary')
    parser.add_argument('--model', help='Absolute path to language model.', default='output_graph.pbmm')
    parser.add_argument('--alphabet', help='Absolute path to alphabet.', default='alphabet.txt')
    parser.add_argument('--trie', help='Absolute path to trie.', default='trie')
    parser.add_argument('--deepspeech-path', help='Absolute path to deepspeech.',
        default='/home/s27688/Documents/repos/eva/deepspeech.service/app/deepspeech-0.5.0-models/')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
