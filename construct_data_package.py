#!/usr/bin/env python

import argparse 

from subprocess import getoutput
from os import listdir
from natsort import natsorted
from math import floor

source_path = './descriptions'
deepspeech_path = './descriptions_output'
package_path ='packaged_data_no_duplicates'

with open(source_path, 'r') as source_file:
    source_lines = source_file.readlines()

with open(deepspeech_path, 'r') as deepspeech_file:
    deepspeech_lines = deepspeech_file.readlines()

packaged_data_list = []
for de, dline in enumerate(deepspeech_lines):
    #newline = dline.replace('\n', '') + '\t' + source_lines[floor(de/14)]
    newDline = dline.replace('\n', '') + '\t'
    newSourceLine = source_lines[floor(de/14)]
    packaged_data_list.append((newDline, newSourceLine))
    
packaged_data_list = list(dict.fromkeys(packaged_data_list))
with open(package_path, 'w') as package_file:
    for line in sorted(packaged_data_list,key=lambda x: len(x[0])):
        package_file.write(str(line[0] + line[1]))
        #package_file.flush()
