# Copyright (C) 2017 - Chenfeng Bao
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License; either version 3 of 
# the License, or (at your option) any later version.
# You should have received a copy of the GNU General Public License 
# along with this program; if not, see <http://www.gnu.org/licenses>.


import json
import argparse
import pprint
pp = pprint.PrettyPrinter(indent=4)


ALPHABET = set(["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","'", "1","2", "3", "4", "5", "6", "7", "8", "9", "0" ])

vocab = []
questions = []
entities = []
translation = {}
new_translations ={}

def query_fuseki():
    pass
# first query fusekt and get response

def join_question_entities():
    elem = "<e>"
    #for now we handle single element type and single element per question
    #later could be different element types such as product, task, and so on
    for question in questions:
        if elem in question:
            for entitie in entities:
                vocab.append(question.replace(elem, entitie))
        else:
            vocab.append(question)

def read_json_files(files):
    for file in files:
        with open(file) as json_file:
            data = json.load(json_file)
            for p in data['results']['bindings']:
                entities.append(p['text']['value'])

def read_question_files(files):
    for file in files:
        with open(file, 'r') as f:
            for line in f:
                questions.append(line)

def read_translation_file(file):
    import ast
    with open(file, 'r') as f:
        s = f.read()
        translation.update(ast.literal_eval(s))
        
def append_from_file(files):
    for file in files:
        with open(file, 'r') as f:
            for line in f:
                vocab.append(line)

def write_new_translation_file(file):
    with open(file, 'w') as f:
        print(new_translations, file=f)

def update_translation_file(file):
    translation.update(new_translations)
    with open(file, 'w') as f:
        print(translation, file=f)

def clean_vocab():
    for ve in range(len(vocab) -1, -1, -1):
        # special case handling bad data from files
        if vocab[ve] == ' ' or len(vocab[ve]) == 0:
            del vocab[ve]
            continue

        ve_lowered = vocab[ve].lower() # keep vocab[ve] to detect all caps
        #remove unwanted chars in this string
        for we, word in enumerate(vocab[ve].split()):
            for ce, char in enumerate(word.lower()):
                if char not in ALPHABET:
                    #this causes new words to be constructed, thus clean_vocab should be called twice
                    ve_lowered = ve_lowered.replace(char, " ")
                    word = word.replace(char, "") # we loose some words here, e.g. r5d) is fine but 300symbolWatts becomes odd

            #if word is all caps, it must be an accronym
            if word.isupper():
                print("found all caps word")
                if isNotPronouncable(word) and word.lower() not in translation:
                    itsTranslation = ' '.join(word[:]).lower()
                    translation.update({word.lower():itsTranslation})
                    new_translations.update({word.lower():itsTranslation})
             
        # maybe we removed some chars, start over on this sentence
        for we, word in enumerate(ve_lowered.split()):
            #check if word is in translation
            if word.lower() in translation:
                #perform translation
                ve_lowered = ve_lowered.replace(word, translation[word])
            if word.isdigit():
                #TODO: add num2word, and add american, i.e. 6600 sixty six hundred
                for ce, char in enumerate(word):
                    if char in translation:
                        ve_lowered = ve_lowered.replace(char, str(" " + translation[char] + " "))
        
        vocab[ve] = ' '.join(ve_lowered.split()) # avoids double space

def isNotPronouncable(word):
    #TODO: implement such that it checks a list, kind of a negative dictionary and checks programḿatically as well.
    if word.lower() != 'air':
        return True


def output_vocab(file):
    with open(file, 'w') as f:
        f.writelines(["%s\n" % item.replace("\n", "")  for item in vocab])
# change digits to letters i.e. 1 one 2 two
# change 50 to fifty
# remove everything in vocab that is not in alphabet
# lower case all
# remove extra white spaces
# remove duplicate lines
# write to vocabulary.txt
def parse_cmdline():
    parser = argparse.ArgumentParser(
        description='Constructs a vocabulary.txt file for kenlm language model generation to be used for deepspeech.\n\
The vocabulary will contain all possible sentences expected from user. All chars not in [a-z],[\'] are removed and\n\
all digits and accronyms are translated.',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-j', '--json', nargs='*',
                       help='List of json files. Should contain entities in ontology, i.e. things in question. e.g. \
                            "Hoist cables". This will be mixed with question templates.',
                        default='file2.json file3.json file4.json')


    parser.add_argument('-t', '--textfiles', nargs='*',
                       help='List of text files. These are only cleaned, translated and ammended to vocabulary.txt')

    parser.add_argument('-q', '--questionfiles', nargs='*',
                       help='List of question files. Each line should contain format "How do I <e> on <e>',
                       default='questions')

    parser.add_argument('-tr', '--translation',
                       help='Dictionary file used for translation. contains dict for "ebm: e b m" and "22: twenty two"',
                       default='translation.dict')

    parser.add_argument('-ut','--update-translation', dest='update_translation', action='store_true', 
        help='Adds all capital letters to dictionary file. Useful for back translation after inference. \
              e.g. EBM is added as "e b m" to vocabulary, this is needed for deepspeech to work but after inference it \
              will predict e b m. Using updated dictionary.txt we can back translate these cases.')
    parser.add_argument('-ntf', '--new-translation-file',
                       help='Dictionary file used for translation. contains dict for "ebm: e b m" and "22: twenty two"',
                       default='new_translation_file')

    parser.add_argument('--no-update-translation', dest='update_translation', action='store_false')

    parser.set_defaults(update_translation=False)

    parser.add_argument('-o', '--output',
                       help='Output file for vocabulary.', default='vocabulary.txt')

    args = parser.parse_args()
    return args

def main():
    args = parse_cmdline()
    read_translation_file(args.translation)
    pp.pprint(translation)
    read_json_files(args.json)
    read_question_files(args.questionfiles)
    if args.textfiles:
        append_from_file(args.textfiles)
    join_question_entities()
    #call twice
    clean_vocab()
    clean_vocab()
    output_vocab(args.output)
    if (len(new_translations) > 0):
        print("New accronyms were detected. See --new-translate-file")
        write_new_translation_file(args.new_translation_file)
        if (args.update_translation):
            update_translation_file(args.translation)

    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nStopped by user')
