#!/usr/bin/env python

# Copyright 2018 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Google Cloud Text-To-Speech API sample application .

Example usage:
    python synthesize_file.py --text resources/hello.txt
    python synthesize_file.py --ssml resources/hello.ssml
"""

import argparse
import concurrent.futures
import urllib.request
import time
from multiprocessing import cpu_count
import os
import threading
import random
import sys


waveNetVoices = (
    "en-AU-Wavenet-A", # Male
    "en-AU-Wavenet-B", # Female
    "en-AU-Wavenet-C",
    "en-AU-Wavenet-D",
    "en-GB-Wavenet-A",
    "en-GB-Wavenet-B",
    "en-GB-Wavenet-C",
    "en-GB-Wavenet-D",
    "en-US-Wavenet-A",
    "en-US-Wavenet-B",
    "en-US-Wavenet-C",
    "en-US-Wavenet-D",
    "en-US-Wavenet-E",
    "en-US-Wavenet-F"
)
from_line = 0
to_line = 2

# [START tts_synthesize_text_file]

def main():

    # make it work nice across threads
    def RateLimited(max_per_second):
      '''
      Decorator that make functions not be called faster than
      '''
      lock = threading.Lock()
      minInterval = 1.0 / float(max_per_second)
      def decorate(func):
        lastTimeCalled = [0.0]
        def rateLimitedFunction(args,*kargs):
          lock.acquire()
          elapsed = time.process_time() - lastTimeCalled[0]
          leftToWait = minInterval - elapsed

          if leftToWait>0:
            time.sleep(leftToWait)
            #print(threading.current_thread().getName() + ': waiting for ' + str(leftToWait))
          lock.release()

          ret = func(args,*kargs)
          lastTimeCalled[0] = time.process_time()
          return ret
        return rateLimitedFunction
      return decorate


    @RateLimited(4.50) #4.5 * 60 = 270/minute
    def synthesize_speech_limited(client, input_text, voice, audio_config, e, voice_index):
        keep_trying = True
        attempt = 0
        while keep_trying:
            try:
                response = client.synthesize_speech(input_text, voice, audio_config)
            except Exception as exc:
                print("synthesize_speech_limited" + exc)
                sys.stdout.flush()
                sleep_time = random.randrange(1, 20) / 10.0
                print(str(threading.current_thread().getName() + " sleeping for " + str(sleep_time) ))
                sys.stdout.flush()
                time.sleep(sleep_time)
                attempt = attempt + 1
                if (attempt > 13):
                    keep_trying = False
                    raise Exception(tuple(exc, input_text, voice, audio_config, e, voice_index))
            else:
                return response


    def write_audio_file(args, line, e):
        """
        give a text, line and e as line number it generates the text to speech of that text
        for all elements in wavenet list. It is the input function for thread.
        """
        e = e + from_line 
        from google.cloud import texttospeech
        client = texttospeech.TextToSpeechClient()
        audio_config = texttospeech.types.AudioConfig(
            sample_rate_hertz=16000,
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16)
        for voice_index in range(len(waveNetVoices)):
            voice = texttospeech.types.VoiceSelectionParams(
                name=waveNetVoices[voice_index],
                language_code='en-AU')

            input_text = texttospeech.types.SynthesisInput(text=line)
            response = synthesize_speech_limited(client, input_text, voice, audio_config, e, voice_index)
            # The response's audio_content is binary.
            filename = 'line' + str(e + 1) + "_" + str(voice_index) + '.wav'
            with open((output_path + filename), 'wb') as out:
                out.write(response.audio_content)
            #update csv file for this audio file, not the best practice because creates shared object
            csv_file_contents.append(str(str(os.path.abspath(output_path + filename)) +","+ str(response.ByteSize()) + "," + line))

        return 'Audio written for line:' + str(e + 1) + " " + line
    
    def synthesize_text_file(args):
        """Synthesizes speech from the input file of text."""

        with open(args.text, 'r') as f:
            lines = f.readlines()

        lines = lines[from_line:to_line] 

        max_workers = cpu_count() * 2 # num of threads per core
        failed_jobs = []
        #from google.cloud import texttospeech
        #client = texttospeech.TextToSpeechClient()
        with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
            start = time.time()
            future_call = {executor.submit(write_audio_file, args, line, e): line for e, line in enumerate(lines)}
            for future in concurrent.futures.as_completed(future_call):
                try:
                    print(future.result())
                    sys.stdout.flush()
                except Exception as exc:
                    print(exc)
                    failed_jobs.append(exc)
                    sys.stdout.flush()
                else:
                    pass
            end = time.time()
            print("execution time: " + str(end - start))
            sys.stdout.flush()
        if len(failed_jobs) == 0:
            print("All jobs passed.")
        else:
            with open((output_path + '/failed_jobs'), 'w') as out:
                print("See failed_jobs file")
                for e in failed_jobs:
                    out.write(str(e))
    # [END tts_synthesize_text_file]
    args = parse_cmdline()
    output_path = args.output_dir + "/"

    if (os.path.exists(output_path)):
        print("Output dir exists, failed_jobs and existing files will be overwritten.")
    else:
        os.mkdir(output_path)

    csv_file_path = args.csv
    csv_file_contents = []
    csv_file_contents.append('wav_filename,wav_filesize,transcript\n')

    synthesize_text_file(args)
    write_list_to_file(csv_file_contents, args.csv)


def write_list_to_file(listname, file):
    with open(file, 'w') as f:
        f.write("".join(listname))

def parse_cmdline():
    parser = argparse.ArgumentParser(
        description='tts, text-to-speech, takes a file containing sentences and output wave files of the google api \
result in output dir',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--text',
                       help='The text file from which to synthesize speech. Each line has a sentence',
                       default='tts_input')

    parser.add_argument('--output-dir',
                       help='Output directory to write files to.',
                       default='wavs')
    parser.add_argument('--csv',
                       help='csv file according to deepspeech expected format.',
                       default='synthesized_wavs.csv')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main() 
